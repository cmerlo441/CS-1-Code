import java.util.Random;

// import edu.ncc.cmerlo.csc120.Rectangle.Rectangle;

public class BoxApp {
    public static void main( String args[] ) {
        Random rng = new Random();
        final int NUM_RECTANGLES = 10;

        Rectangle rectangles[] = new Rectangle[ NUM_RECTANGLES ];
        for( int i = 0; i < rectangles.length; ++i ) {
            if( rng.nextBoolean() ) {
                rectangles[ i ] = new Rectangle();
            } else {
                rectangles[ i ] = new Box();
            }
        }

        for (Rectangle rectangle : rectangles) {
            displayRectangle( rectangle );
        }
    }

    public static void displayRectangle( Rectangle r ) {
        System.out.println( "Length: " + r.getLength() );
        System.out.println( "Width: " + r.getWidth() );
        if( r instanceof Box ) {
            Box b = ( Box )r;
            System.out.println( "Depth: " + b.getDepth() );
        }
    }
    
}