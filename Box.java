// package edu.ncc.cmerlo.csc120.Rectangle;

/**
 * Description: Stores a representation of a Rectangle
 * 
 * @author Prof. Christopher R. Merlo &lt;cmerlo@ncc.edu&gt;
 * @version 2020-01
 *
 */
public class Box extends Rectangle {
    private int depth;

    /**
     * Default constructor
     */
    public Box() {
        super();
        depth = 6;
    }

    /**
     * Parameterized constructor
     * Uses default values from parent class
     * @param d Depth of the box
     */
    public Box( int d ) {
        this();
        setDepth( d );
    }

    /**
     * Parameterized constructor
     * @param l Length of the box
     * @param w Width of the box
     * @param d Depth of the box
     */
    public Box( int l, int w, int d ) {
        super( l, w );
        setDepth( depth );
    }

    /**
     * Mutator
     * @param d Depth of the box
     */
    public void setDepth( int d ){
        if( d > 0 ) {
            depth = d;
        }
    }

    /**
     * Accessor
     * @return Depth of the box
     */
    public int getDepth(){
        return depth;
    }

    /**
     * toString
     * @return A String representation of the object's state
     */
    @Override
    public String toString(){
        // Notice that super. is required before toString here
        // to make sure we're calling the one from Rectangle
        return String.format( "%s x %d", super.toString(), depth );
    }
}